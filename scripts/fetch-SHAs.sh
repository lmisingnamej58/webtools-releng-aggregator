#!/bin/bash
#
echo Collect the SHAs used in the latest builds to determine if we've built everything upstream already

# compare https://git.eclipse.org/c/jeetools/webtools.javaee.git/commit/?h=master or ?h=R3_9_maintenance
# <tr><th>commit</th><td colspan='2' class='sha1'><a href='/c/jeetools/webtools.javaee.git/commit/?id=7500821ec81657bafc1c6411d79547266d7befb8'>7500821ec81657bafc1c6411d79547266d7befb8</a> (<a href='/c/jeetools/webtools.javaee.git/patch/?id=7500821ec81657bafc1c6411d79547266d7befb8'>patch</a>)</td></tr>

# with  https://ci.eclipse.org/webtools/job/webtools-javaee_master/lastStableBuild/api/xml?xpath=//lastBuiltRevision/SHA1
# <SHA1>7500821ec81657bafc1c6411d79547266d7befb8</SHA1>

norm="\033[0;39m"
green="\033[1;32m"
red="\033[1;31m"

branch=master
quiet=0 # flag which only returns status of 0/"" (if pass) or 1 (if fail)
projects="common servertools jsdt sourceediting javaee dali jsf webservices"

# read commandline args
while [[ "$#" -gt 0 ]]; do
  case $1 in
    '-b') branch="$2"; shift 1;;
    '-q') quiet=1; shift 0;;
    '-nc') red=$norm; green=$norm; shift 0;; # disable colour
    *) projects="$projects $1";;
  esac
  shift 1
done

if [[ ! $branch ]]; then 
  echo "branch is not set!"; exit 1
fi
if [[ ! $projects ]]; then 
  echo "no project(s) selected!"; exit 1
fi

log () 
{
	if [[ $quiet == 0 ]]; then echo -e "$1"; fi
}
logn () 
{
	if [[ $quiet == 0 ]]; then echo -e -n "$1"; fi
}

projectsToBuild=""
for d in ${projects}; do 
  if [[ ${d} == "common" ]]; then 
    gitURL="https://git.eclipse.org/c/webtools-common/webtools.common.git/commit/?h=${branch}"
  elif [[ ${d} == "javaee" ]]; then
    gitURL="https://git.eclipse.org/c/jeetools/webtools.javaee.git/commit/?h=${branch}"
  elif [[ ${d} == "jsdt" ]]; then
    gitURL="https://github.com/eclipse-jsdt/webtools.jsdt/commits/"${branch}"
  elif [[ ${d} == "sourceediting" ]]; then
    gitURL="https://github.com/eclipse-sourceediting/sourceediting/commits/"${branch}"
  elif [[ ${d} == "webservices" ]]; then
    gitURL="https://github.com/eclipse-webservices/webservices/commits/"${branch}"
  elif [[ ${d} == "servertools" ]]; then
    gitURL="https://github.com/eclipse-servertools/servertools/commits/"${branch}"
  else
    gitURL="https://git.eclipse.org/c/${d}/webtools.${d}.git/commit/?h=${branch}"
  fi
  # use lastSuccessfulBuild to allow for failed tests (yellow builds)
  jobURL="https://ci.eclipse.org/webtools/job/webtools-${d}_${branch}/lastSuccessfulBuild/api/xml?xpath=//lastBuiltRevision/SHA1"

  jobSHA=$(wget -q ${jobURL} -O- | sed -e "s#.*<SHA1>\(.\+\)</SHA1>.*#\1#")
  logn "$d :: $jobSHA (last successful job)"

  if [[ ${d} == "jsdt" ]]; then
    gitSHA=$(curl $gitURL|grep '\/commit\/.*permalink'|cut -f5- -d\/|cut -f1 -d\")
  else
    gitSHA=$(wget -q ${gitURL} -O- | grep "<th>commit</th>" | sed -e "s#.*?id=\(.\+\)'>.*#\1#")
  fi

  if [[ ${gitSHA} !=  ${jobSHA} ]]; then
    bwpURL="https://ci.eclipse.org/webtools/job/webtools-${d}_${branch}/buildWithParameters?token=CI-BUILD"
    if [[ $quiet == 0 ]]; then
      projectsToBuild="${projectsToBuild} <a href=${bwpURL}>${bwpURL}</a>"
    else
      projectsToBuild="${projectsToBuild} ${bwpURL}"
    fi
    logn " :: ${red}ERROR${norm}"
    if [[ ${gitSHA} ]]; then
      log ": $gitSHA (last git commit)"
    else
      log ": $gitURL"
    fi
  else
    log " :: ${green}OK${norm}"
  fi
done

if [[ ${projectsToBuild} != "" ]]; then 
  log ""
  logn "[ERROR] These projects need to be built upstream before we can rebuild the CI job: "
  echo ${projectsToBuild}
fi
